# floodgate

A customizable, standalone Discord Gateway client that gives you control of what gets processed. Maximum efficiency!